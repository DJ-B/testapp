// Mamy taką strukturę grup i userów
//
// ALL
//   ADMIN
//     user1
//     user2
//     user3
//   USERS
//     user4
//     user5
//     HR
//      user6
//      user7
//      user8
//     DEVS
//       user9
//       user10
//       user11
//
// Chcemy uzyskać takie wyniki:
// user1, user2, user3, user4, user5, user6, user7, user8, user9, user10, user11
//
// Mamy interfejsy:

import { Observable } from 'rxjs';

export interface Group {
  name: string;
}

export interface User {
  name: string;
}

export interface GroupUsers {
  getSubGroups(groupName: string): Observable<Group[]>;
  getUsersFromGroup(groupName: string): Observable<User[]>;
}
